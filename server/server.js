const { Pool } = require('pg')
const express = require('express');

const app = express();
app.use(express.urlencoded());

const pool = new Pool({
  host: 'localhost',
  user: 'postgres',
  password: 'adv97682371',
  database: 'postgres',
  max: 10,
  idleTimeoutMillis: 30000,
});


app.get('/location', function (req, res) {
  pool.connect(function (err, client, done) {
    if (err) {
      console.log("not able to get connection " + err);
      res.status(400).send(err);
    }

    const ln = req.query.ln;

    client.query('SELECT status_code FROM location WHERE location.location_name = \'' + ln + '\'', function (err, result) {
      console.log(ln)
      //call `done()` to release the client back to the pool
      done();
      if (err) {
        console.log(err);
        res.status(400).send(err);
      }
      console.log(result.rows);
      res.status(200).send(result.rows);
    });
  });
});


const port = process.env.PORT || 1337;
app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Listening on ${port}`);
});