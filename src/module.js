/* global _, window */
import config from 'app/core/config';
import { PanelCtrl } from 'app/plugins/sdk';
import angular from 'angular';

import './Cesium/Widgets/widgets.css!';
import './css/3d-globe-panel.css!';

require('./Cesium/Cesium.js');

const BASE_URL = `${config.appSubUrl}/public/plugins/satellogic-3d-globe-panel`;
const Cesium = window.Cesium;
const panelSettings = {
  url: '',
  bingKey: 'AjuoFPhKBhTBUgRw3hCFB--3jt3oAoObLM2aJ50BFOJrdUPoGIQjVWj8dRrF5N6Z',
};
window.CESIUM_BASE_URL = `${BASE_URL}/Cesium/`;

// Main Globe Control
export class GlobeCtrl extends PanelCtrl {
  constructor($scope, $injector, $rootScope) {
    super($scope, $injector);
    this.rootScope = $rootScope;
    this.scope = $scope;

    this.panel = _.defaults(this.panel, panelSettings);
    this.timeSrv = $injector.get('timeSrv');
    this.templateSrv = $injector.get('templateSrv');

    this.events.on('refresh', this.refresh);
    this.events.on('init-edit-mode', this.onInitEditMode);
    this.events.on('render', this.updateViewerHeight);
    this.rootScope.onAppEvent('setCrosshair', this.updateCurrentTime, $scope);

    // Override Cesium timeline labels to use UTC or Browser timezone
    this.overrideTimelineLabels(this.scope.ctrl.dashboard);

    this.refresh();
  }
  overrideTimelineLabels = (dashboard) => {
    // Override Cesium Timeline.makelabel to transform dates from UTC.
    // This code is a strict port from https://github.com/AnalyticalGraphicsInc/cesium/blob/e786760dee9afb77493ddf624ccc31493c24084b/Source/Widgets/Timeline/Timeline.js#L317
    // with a few tweaks to shift the displayed time label to the correct tz.
    Cesium.Timeline.prototype.makeLabel = function makeLabel(_time) {
      const timelineMonthNames = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
      ];
      let millisecondString = ' UTC';
      let time = _time;
      const tzOffset = (new Date()).getTimezoneOffset();
      const twoDigits = num => (
        (num < 10) ? (`0${num.toString()}`) : num.toString()
      );

      if (!dashboard.isTimezoneUtc() && tzOffset !== 0) {
        time = Cesium.JulianDate.addMinutes(
          time,
          -1 * tzOffset,
          new Cesium.JulianDate()
        );

        millisecondString = ` UTC${tzOffset > 0 ? '-' : '+'}` +
          `${twoDigits(Math.floor(tzOffset / 60))}:` +
          `${twoDigits(tzOffset % 60)}`;
      }
      const gregorian = Cesium.JulianDate.toGregorianDate(time);
      const millisecond = gregorian.millisecond;

      // eslint-disable-next-line no-underscore-dangle
      if ((millisecond > 0) && (this._timeBarSecondsSpan < 3600)) {
        millisecondString = Math.floor(millisecond).toString();
        while (millisecondString.length < 3) {
          millisecondString = `0${millisecondString}`;
        }
        millisecondString = `.  ${millisecondString}`;
      }
      // eslint-disable-next-line max-len
      return `${timelineMonthNames[gregorian.month - 1]} ${gregorian.day} ${gregorian.year} ${twoDigits(gregorian.hour)}:${twoDigits(gregorian.minute)}:${twoDigits(gregorian.second)}${millisecondString}`;
    };
  }
  onInitEditMode = () => {
    this.addEditorTab(
      'Sources',
      'public/plugins/satellogic-3d-globe-panel/sources.html',
      2,
    );
    this.addEditorTab(
      'My Custom Tab',
      'public/plugins/satellogic-3d-globe-panel/customizations.html',
      3,
    );
  }
  registerViewer = (viewer) => {
    this.viewer = viewer;
    this.updateViewerTimespan();
  }
  updateViewerTimespan = () => {
    let { from, to } = this.timeSrv.timeRange();
    from = Cesium.JulianDate.fromDate(
      from.toDate(),
      new Cesium.JulianDate()
    );
    to = Cesium.JulianDate.fromDate(
      to.toDate(),
      new Cesium.JulianDate()
    );
    // Update Cesium clock timespan
    this.viewer.clock.startTime = from;
    this.viewer.clock.stopTime = to;

    // Zoom timeline to include the whole timespan
    this.viewer.timeline.zoomTo(from, to);
  }
  updateViewerHeight = () => {
    const west = this.templateSrv.replace(`${this.panel.west}`);
    const south = this.templateSrv.replace(`${this.panel.south}`);
    const east = this.templateSrv.replace(`${this.panel.east}`);
    const north = this.templateSrv.replace(`${this.panel.north}`);

    const rectangle = Cesium.Rectangle.fromDegrees(west, south, east, north);

    Cesium.Camera.DEFAULT_VIEW_FACTOR = 0;
    Cesium.Camera.DEFAULT_VIEW_RECTANGLE = rectangle;

    // Cesium needs a fixed height to correctly size its canvas
    this.viewer.container.style.height = `${Math.ceil(this.height)}px`;

    // Add a small delay to ensure the rectangle is correclty displayed
    window.setTimeout(() => {
      this.viewer.camera.setView({
        destination: rectangle,
      });
    }, 100);
  }
  updateCurrentTime = (event, info) => {
    if (this.scope.ctrl.dashboard.sharedCrosshair) {
      this.viewer.clock.currentTime = Cesium.JulianDate.fromDate(
        new Date(info.pos.x),
        new Cesium.JulianDate()
      );
    }
  }
  refresh = () => {
    if (this.panel.bingKey) {
      Cesium.BingMapsApi.defaultKey = this.panel.bingKey;
    }
    if (this.viewer && this.panel.url) {
      // Get global from/to
      const { from, to } = this.timeSrv.timeRange();

      // Replace querystring with dashboard's templates and from/to
      const dsOneUrl = this.templateSrv.replace(
        `${this.panel.url}?${this.panel.query}`,
        this.panel.scopedVars,
        'pipe'
      ).replace(/\$timeFrom/g, +from)
        .replace(/\$timeTo/g, +to);

      const dsTwoUrl = this.templateSrv.replace(
        `${this.panel.url2}?${this.panel.query2}`,
        this.panel.scopedVars,
        'pipe'
      ).replace(/\$timeFrom/g, +from)
        .replace(/\$timeTo/g, +to);

      const dsThreeUrl = this.templateSrv.replace(
        `${this.panel.url3}?${this.panel.query3}`,
        this.panel.scopedVars,
        'pipe'
      ).replace(/\$timeFrom/g, +from)
        .replace(/\$timeTo/g, +to);

      // Grab the HTML element so we can attach the event listener
      // and apply callback funtion to hide show datasources
      // Need to refactor to use single callback function for all sources
      const checkbox = window.document.getElementById('source1');
      const checkboxTwo = window.document.getElementById('source2');
      const checkboxThree = window.document.getElementById('source3');

      checkboxThree.addEventListener('change', function () {
        // Checkbox state changed.
        if (checkboxThree.checked = true &&
          !this.viewer.dataSources.contains(dsThree)) {
          // Show if not shown.
          this.viewer.dataSources.add(dsThree);
        } else {
          // Hide if currently shown.
          this.viewer.dataSources.remove(dsThree);
        }
      }.bind(this), false);

      checkboxTwo.addEventListener('change', function () {
        // Checkbox state changed.
        if (checkboxTwo.checked = true &&
          !this.viewer.dataSources.contains(dsTwo)) {
          // Show if not shown.
          this.viewer.dataSources.add(dsTwo);
        } else {
          // Hide if currently shown.
          this.viewer.dataSources.remove(dsTwo);
        }
      }.bind(this), false);

      checkbox.addEventListener('change', function () {
        // Checkbox state changed.
        if (checkbox.checked = true &&
          !this.viewer.dataSources.contains(dsOne)) {
          // Show if not shown.
          this.viewer.dataSources.add(dsOne);
        } else {
          // Hide if currently shown.
          this.viewer.dataSources.remove(dsOne);
        }
      }.bind(this), false);

      // Create and load your data sources
      const dsOne = new Cesium.CzmlDataSource();
      const dsTwo = new Cesium.KmlDataSource();
      const dsThree = new Cesium.KmlDataSource();

      // Need to write error handling to apply to all sources

      // Load data and handle once promise returns
      dsOne.load(dsOneUrl).then(function (dataSource) {
        this.viewer.dataSources.add(dataSource);
        const entities = dataSource.entities.values;

        for (let i = 0; i < entities.length; i++) {
          const entity = entities[i];

          const xhr = new XMLHttpRequest();
          const params = '?ln=';
          const locationName = entity.name;
          const url = 'http://18.232.81.218:1337/location' + params + locationName;

          xhr.responseType = 'json';

          xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE) {
              if (xhr.response[0].status_code === 1) {
                entity.billboard.image._value = 'http://18.232.81.218/images/png/redCircle.png';
              } else if (xhr.response[0].status_code === 2) {
                entity.billboard.image._value = 'http://18.232.81.218/images/png/orangeCircle.png';
              } else {
                entity.billboard.image._value = 'http://18.232.81.218/images/png/greenCircle.png';
              }
            }
          }

          xhr.open('GET', url);
          xhr.send();
        }
      }.bind(this), false);

      dsTwo.load(dsTwoUrl);
      dsThree.load(dsThreeUrl, {
        clampToGround: true
      });

      this.viewer.dataSources.add(dsTwo);
      this.viewer.dataSources.add(dsThree).then(

        null,
        (err) => {
          this.rootScope.appEvent(
            'alert-error',
            ['Plugin Error', `Error while retrieving KML data: ${err}`],
          );
        },
      );
    } else if (this.viewer) {
      // Hook Cesium timespan to Grafana's
      this.updateViewerTimespan();
    }
  }
}

GlobeCtrl.templateUrl = 'module.html';

const imageryViewModels = [];

imageryViewModels.push(new Cesium.ProviderViewModel({
  name: 'Bing Maps Aerial',
  iconUrl: Cesium.buildModuleUrl('Widgets/Images/ImageryProviders/bingAerial.png'),
  tooltip: 'Bing Maps aerial imagery \nhttp://www.bing.com/maps',
  creationFunction: function () {
    return new Cesium.BingMapsImageryProvider({
      url: 'https://dev.virtualearth.net',
      key: 'AjuoFPhKBhTBUgRw3hCFB--3jt3oAoObLM2aJ50BFOJrdUPoGIQjVWj8dRrF5N6Z',
      mapStyle: Cesium.BingMapsStyle.AERIAL
    });
  }
}));

imageryViewModels.push(new Cesium.ProviderViewModel({
  name: 'Bing Maps Aerial with Labels',
  iconUrl: Cesium.buildModuleUrl('Widgets/Images/ImageryProviders/bingAerialLabels.png'),
  tooltip: 'Bing Maps aerial imagery with label overlays \nhttp://www.bing.com/maps',
  creationFunction: function () {
    return new Cesium.BingMapsImageryProvider({
      url: 'https://dev.virtualearth.net',
      key: 'AjuoFPhKBhTBUgRw3hCFB--3jt3oAoObLM2aJ50BFOJrdUPoGIQjVWj8dRrF5N6Z',
      mapStyle: Cesium.BingMapsStyle.AERIAL_WITH_LABELS
    });
  }
}));

imageryViewModels.push(new Cesium.ProviderViewModel({
  name: 'Open\u00adStreet\u00adMap',
  iconUrl: Cesium.buildModuleUrl('Widgets/Images/ImageryProviders/openStreetMap.png'),
  tooltip: 'OpenStreetMap (OSM) is a collaborative project to create a free editable \
map of the world.\nhttp://www.openstreetmap.org',
  creationFunction: function () {
    return Cesium.createOpenStreetMapImageryProvider({
      url: 'https://a.tile.openstreetmap.org/'
    });
  }
}));

imageryViewModels.push(new Cesium.ProviderViewModel({
  name: 'Natural Earth\u00a0II',
  iconUrl: Cesium.buildModuleUrl('Widgets/Images/ImageryProviders/naturalEarthII.png'),
  tooltip: 'Natural Earth II, darkened for contrast.\nhttp://www.naturalearthdata.com/',
  creationFunction: function () {
    return Cesium.createTileMapServiceImageryProvider({
      url: Cesium.buildModuleUrl('Assets/Textures/NaturalEarthII')
    });
  }
}));

imageryViewModels.push(new Cesium.ProviderViewModel({
  name: 'Black Marble',
  iconUrl: Cesium.buildModuleUrl('Widgets/Images/ImageryProviders/blackMarble.png'),
  tooltip: 'The lights of cities and villages trace the outlines of civilization in this global view of the Earth at night as seen by NASA/NOAA\'s Suomi NPP satellite.',
  creationFunction: function () {
    return Cesium.createTileMapServiceImageryProvider({
      url: 'https://cesiumjs.org/blackmarble',
      credit: 'Black Marble imagery courtesy NASA Earth Observatory',
      flipXY: true
    });
  }
}));

imageryViewModels.push(new Cesium.ProviderViewModel({
  name: 'ESRI World Imagery',
  iconUrl: Cesium.buildModuleUrl('Widgets/Images/ImageryProviders/esriWorldImagery.png'),
  tooltip: '\
World Imagery provides one meter or better satellite and aerial imagery in many parts of the world and lower resolution \
satellite imagery worldwide.  The map includes NASA Blue Marble: Next Generation 500m resolution imagery at small scales \
(above 1:1,000,000), i-cubed 15m eSAT imagery at medium-to-large scales (down to 1:70,000) for the world, and USGS 15m Landsat \
imagery for Antarctica. The map features 0.3m resolution imagery in the continental United States and 0.6m resolution imagery in \
parts of Western Europe from DigitalGlobe. In other parts of the world, 1 meter resolution imagery is available from GeoEye IKONOS, \
i-cubed Nationwide Prime, Getmapping, AeroGRID, IGN Spain, and IGP Portugal.  Additionally, imagery at different resolutions has been \
contributed by the GIS User Community.\nhttp://www.esri.com',
  creationFunction: function () {
    return new Cesium.ArcGisMapServerImageryProvider({
      url: '//services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer'
    });
  }
}));

imageryViewModels.push(new Cesium.ProviderViewModel({
  name: 'ESRI National Geographic',
  iconUrl: Cesium.buildModuleUrl('Widgets/Images/ImageryProviders/esriNationalGeographic.png'),
  tooltip: '\
World Imagery provides one meter or better satellite and aerial imagery in many parts of the world and lower resolution \
satellite imagery worldwide.  The map includes NASA Blue Marble: Next Generation 500m resolution imagery at small scales \
(above 1:1,000,000), i-cubed 15m eSAT imagery at medium-to-large scales (down to 1:70,000) for the world, and USGS 15m Landsat \
imagery for Antarctica. The map features 0.3m resolution imagery in the continental United States and 0.6m resolution imagery in \
parts of Western Europe from DigitalGlobe. In other parts of the world, 1 meter resolution imagery is available from GeoEye IKONOS, \
i-cubed Nationwide Prime, Getmapping, AeroGRID, IGN Spain, and IGP Portugal.  Additionally, imagery at different resolutions has been \
contributed by the GIS User Community.\nhttp://www.esri.com',
  creationFunction: function () {
    return new Cesium.ArcGisMapServerImageryProvider({
      url: '//services.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer'
    });
  }
}));

// Register the globe directive
angular.module('grafana.directives')
  .directive('gfSatellogic3dGlobe', () => ({
    template: '<div class="cesiumContainer"></div>',
    replace: true,
    restrict: 'E',
    link: (scope, element) => {
      const STARS_FOLDER = `${BASE_URL}/Cesium/Assets/Textures`;
      const viewer = new Cesium.Viewer(element[0], {
        sceneMode: Cesium.SceneMode.SCENE3D,
        imageryProviderViewModels: imageryViewModels,
        baseLayerPicker: true,
        sceneModePicker: false,
        skyBox: new Cesium.SkyBox({
          sources: {
            positiveX: `${STARS_FOLDER}/SkyBox/tycho2t3_80_px.jpg`,
            negativeX: `${STARS_FOLDER}/SkyBox/tycho2t3_80_mx.jpg`,
            positiveY: `${STARS_FOLDER}/SkyBox/tycho2t3_80_py.jpg`,
            negativeY: `${STARS_FOLDER}/SkyBox/tycho2t3_80_my.jpg`,
            positiveZ: `${STARS_FOLDER}/SkyBox/tycho2t3_80_pz.jpg`,
            negativeZ: `${STARS_FOLDER}/SkyBox/tycho2t3_80_mz.jpg`,
          },
        }),
        animation: false,
        geocoder: false,
        shadows: true,
      });

      // Make lighting permanent and not fade out with zoom distance
      viewer.scene.globe.enableLighting = true;
      viewer.scene.globe.lightingFadeOutDistance = 65000000.0;

      // Wait for the first imagery layer to be ready before zooming out
      viewer.imageryLayers.get(0).imageryProvider.readyPromise.then(() => {
        scope.ctrl.updateViewerHeight();
      });

      scope.ctrl.registerViewer(viewer);

    },
  }));

export { GlobeCtrl as PanelCtrl };
